# Biography
Biography is the mod for Crusader Kings III. It automatically records each character's history. All you have to do is pointing to her/his portrait.

![Sample Image](https://i.imgur.com/S8QtK7y.jpg)

## Note
You can't unlock achievements. You can't see any dead character's history.

## Tips
You can keep the history after the death, if the character was pinned to Outliner at the death.

## Supported Languages
- English
- French (by Qahnaariin)
- German (by Theyn_T)
- Chinese (by Juijote)
- Russian (by imker)
- also, you can use the mod in English with Spanish and Korean environments.

## Translators Needed
If you want to help me, please translate the file `/localization/(your language)/chronicle_l_(your language).yml` and send a pull/merge request. All you have to translate is the first section only. Please see the file for details.
