﻿# game_start.txt

on_game_start_after_lobby = {
	on_actions = {
		chronicle_on_game_start_after_lobby
	}
}

chronicle_on_game_start_after_lobby = {
	events = {
		chronicle.0002	# Called from code after history generation
	}
}


# yearly_on_actions.txt

yearly_playable_pulse = {
	on_actions = {
		chronicle_yearly_playable_pulse
	}
}

chronicle_yearly_playable_pulse = {
	events = {
		chronicle.0005	# Called from code once a year for "playable" (count+) characters
	}
}


# death.txt

on_death = {
	on_actions = {
		chronicle_on_death
	}
}

chronicle_on_death = {
	events = {
		chronicle.0006	# Character just about to die
	}
}


# title_on_actions.txt

on_title_gain = {
	on_actions = {
		chronicle_on_title_gain
	}
}

on_title_lost = {
	on_actions = {
		chronicle_on_title_lost
	}
}

chronicle_on_title_gain = {
	events = {
		delay = { days = 1 }
		chronicle.0003	# A title is transferred to a new character
	}
}

chronicle_on_title_lost = {
	events = {
		delay = { days = 1 }
		chronicle.0004	# A title is lost by a character
	}
}


# war_on_actions.txt

on_war_won_attacker = {
	on_actions = {
		chronicle_on_war_won_attacker
	}
}

on_war_won_defender = {
	on_actions = {
		chronicle_on_war_won_defender
	}
}

on_war_white_peace = {
	on_actions = {
		chronicle_on_war_white_peace
	}
}

chronicle_on_war_won_attacker = {
	effect = {
		chronicle_war_end_attacker_win_effect = yes
	}
}

chronicle_on_war_won_defender = {
	effect = {
		chronicle_war_end_defender_win_effect = yes
	}
}

chronicle_on_war_white_peace = {
	effect = {
		chronicle_war_end_white_peace_effect = yes
	}
}


# marriage_concubinage.txt

on_marriage = {
	on_actions = {
		chronicle_on_marriage
	}
}

on_divorce = {
	on_actions = {
		chronicle_on_divorce
	}
}

on_concubinage = {
	on_actions = {
		chronicle_on_concubinage
	}
}

on_concubinage_end = {
	on_actions = {
		chronicle_on_concubinage_end
	}
}

chronicle_on_marriage = {
	effect = {
		chronicle_family_spouse_change_effect = { TARGET = scope:spouse FAMILY = spouse GAIN_OR_LOSE = gain }
	}
}

chronicle_on_divorce = {
	effect = {
		chronicle_family_spouse_change_effect = { TARGET = scope:spouse FAMILY = spouse GAIN_OR_LOSE = lose }
	}
}

chronicle_on_concubinage = {
	effect = {
		chronicle_family_concubine_change_effect = { TARGET = scope:concubine FAMILY = concubine GAIN_OR_LOSE = gain }
	}
}

chronicle_on_concubinage_end = {
	effect = {
		chronicle_family_concubine_change_effect = { TARGET = scope:concubine FAMILY = concubine GAIN_OR_LOSE = lose }
	}
}


# child_birth_on_actions.txt

on_birth_child = {
	on_actions = {
		chronicle_on_birth_child
	}
}

chronicle_on_birth_child = {
	effect = {
		chronicle_family_child_change_effect = { TARGET = scope:child FAMILY = child GAIN_OR_LOSE = gain }
	}
}


# relation_on_actions.txt

on_set_relation_friend = {
	on_actions = {
		chronicle_on_set_relation_friend
	}
}

on_remove_relation_friend = {
	on_actions = {
		chronicle_on_remove_relation_friend
	}
}

on_set_relation_rival = {
	on_actions = {
		chronicle_on_set_relation_rival
	}
}

on_remove_relation_rival = {
	on_actions = {
		chronicle_on_remove_relation_rival
	}
}

chronicle_on_set_relation_friend = {
	effect = {
		chronicle_relation_change_effect = { RELATION = friend GAIN_OR_LOSE = gain }
	}
}

chronicle_on_remove_relation_friend = {
	effect = {
		chronicle_relation_change_effect = { RELATION = friend GAIN_OR_LOSE = lose }
	}
}

chronicle_on_set_relation_rival = {
	effect = {
		chronicle_relation_change_effect = { RELATION = rival GAIN_OR_LOSE = gain }
	}
}

chronicle_on_remove_relation_rival = {
	effect = {
		chronicle_relation_change_effect = { RELATION = rival GAIN_OR_LOSE = lose }
	}
}
