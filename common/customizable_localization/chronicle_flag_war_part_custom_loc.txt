﻿ChronicleFlagWarPart1 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c1
			var:flag_c1 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c1
			var:flag_c1 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart2 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c2
			var:flag_c2 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c2
			var:flag_c2 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart3 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c3
			var:flag_c3 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c3
			var:flag_c3 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart4 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c4
			var:flag_c4 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c4
			var:flag_c4 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart5 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c5
			var:flag_c5 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c5
			var:flag_c5 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart6 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c6
			var:flag_c6 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c6
			var:flag_c6 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart7 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c7
			var:flag_c7 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c7
			var:flag_c7 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart8 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c8
			var:flag_c8 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c8
			var:flag_c8 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart9 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c9
			var:flag_c9 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c9
			var:flag_c9 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart10 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c10
			var:flag_c10 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c10
			var:flag_c10 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart11 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c11
			var:flag_c11 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c11
			var:flag_c11 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart12 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c12
			var:flag_c12 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c12
			var:flag_c12 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart13 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c13
			var:flag_c13 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c13
			var:flag_c13 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart14 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c14
			var:flag_c14 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c14
			var:flag_c14 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart15 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c15
			var:flag_c15 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c15
			var:flag_c15 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart16 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c16
			var:flag_c16 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c16
			var:flag_c16 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart17 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c17
			var:flag_c17 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c17
			var:flag_c17 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart18 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c18
			var:flag_c18 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c18
			var:flag_c18 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart19 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c19
			var:flag_c19 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c19
			var:flag_c19 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart20 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c20
			var:flag_c20 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c20
			var:flag_c20 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart21 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c21
			var:flag_c21 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c21
			var:flag_c21 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart22 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c22
			var:flag_c22 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c22
			var:flag_c22 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart23 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c23
			var:flag_c23 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c23
			var:flag_c23 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart24 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c24
			var:flag_c24 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c24
			var:flag_c24 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart25 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c25
			var:flag_c25 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c25
			var:flag_c25 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart26 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c26
			var:flag_c26 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c26
			var:flag_c26 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart27 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c27
			var:flag_c27 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c27
			var:flag_c27 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart28 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c28
			var:flag_c28 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c28
			var:flag_c28 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart29 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c29
			var:flag_c29 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c29
			var:flag_c29 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart30 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c30
			var:flag_c30 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c30
			var:flag_c30 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart31 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c31
			var:flag_c31 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c31
			var:flag_c31 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart32 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c32
			var:flag_c32 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c32
			var:flag_c32 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart33 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c33
			var:flag_c33 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c33
			var:flag_c33 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart34 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c34
			var:flag_c34 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c34
			var:flag_c34 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart35 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c35
			var:flag_c35 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c35
			var:flag_c35 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart36 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c36
			var:flag_c36 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c36
			var:flag_c36 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart37 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c37
			var:flag_c37 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c37
			var:flag_c37 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart38 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c38
			var:flag_c38 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c38
			var:flag_c38 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart39 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c39
			var:flag_c39 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c39
			var:flag_c39 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart40 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c40
			var:flag_c40 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c40
			var:flag_c40 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart41 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c41
			var:flag_c41 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c41
			var:flag_c41 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart42 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c42
			var:flag_c42 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c42
			var:flag_c42 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart43 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c43
			var:flag_c43 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c43
			var:flag_c43 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart44 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c44
			var:flag_c44 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c44
			var:flag_c44 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart45 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c45
			var:flag_c45 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c45
			var:flag_c45 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart46 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c46
			var:flag_c46 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c46
			var:flag_c46 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart47 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c47
			var:flag_c47 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c47
			var:flag_c47 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart48 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c48
			var:flag_c48 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c48
			var:flag_c48 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart49 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c49
			var:flag_c49 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c49
			var:flag_c49 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart50 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c50
			var:flag_c50 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c50
			var:flag_c50 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart51 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c51
			var:flag_c51 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c51
			var:flag_c51 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart52 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c52
			var:flag_c52 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c52
			var:flag_c52 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart53 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c53
			var:flag_c53 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c53
			var:flag_c53 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart54 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c54
			var:flag_c54 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c54
			var:flag_c54 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart55 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c55
			var:flag_c55 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c55
			var:flag_c55 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart56 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c56
			var:flag_c56 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c56
			var:flag_c56 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart57 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c57
			var:flag_c57 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c57
			var:flag_c57 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart58 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c58
			var:flag_c58 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c58
			var:flag_c58 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart59 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c59
			var:flag_c59 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c59
			var:flag_c59 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart60 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c60
			var:flag_c60 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c60
			var:flag_c60 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart61 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c61
			var:flag_c61 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c61
			var:flag_c61 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart62 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c62
			var:flag_c62 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c62
			var:flag_c62 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart63 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c63
			var:flag_c63 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c63
			var:flag_c63 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart64 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c64
			var:flag_c64 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c64
			var:flag_c64 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart65 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c65
			var:flag_c65 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c65
			var:flag_c65 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart66 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c66
			var:flag_c66 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c66
			var:flag_c66 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart67 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c67
			var:flag_c67 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c67
			var:flag_c67 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart68 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c68
			var:flag_c68 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c68
			var:flag_c68 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart69 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c69
			var:flag_c69 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c69
			var:flag_c69 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart70 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c70
			var:flag_c70 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c70
			var:flag_c70 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart71 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c71
			var:flag_c71 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c71
			var:flag_c71 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart72 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c72
			var:flag_c72 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c72
			var:flag_c72 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart73 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c73
			var:flag_c73 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c73
			var:flag_c73 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart74 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c74
			var:flag_c74 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c74
			var:flag_c74 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart75 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c75
			var:flag_c75 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c75
			var:flag_c75 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart76 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c76
			var:flag_c76 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c76
			var:flag_c76 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart77 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c77
			var:flag_c77 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c77
			var:flag_c77 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart78 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c78
			var:flag_c78 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c78
			var:flag_c78 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart79 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c79
			var:flag_c79 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c79
			var:flag_c79 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart80 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c80
			var:flag_c80 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c80
			var:flag_c80 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart81 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c81
			var:flag_c81 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c81
			var:flag_c81 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart82 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c82
			var:flag_c82 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c82
			var:flag_c82 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart83 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c83
			var:flag_c83 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c83
			var:flag_c83 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart84 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c84
			var:flag_c84 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c84
			var:flag_c84 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart85 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c85
			var:flag_c85 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c85
			var:flag_c85 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart86 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c86
			var:flag_c86 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c86
			var:flag_c86 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart87 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c87
			var:flag_c87 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c87
			var:flag_c87 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart88 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c88
			var:flag_c88 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c88
			var:flag_c88 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart89 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c89
			var:flag_c89 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c89
			var:flag_c89 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart90 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c90
			var:flag_c90 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c90
			var:flag_c90 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart91 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c91
			var:flag_c91 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c91
			var:flag_c91 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart92 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c92
			var:flag_c92 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c92
			var:flag_c92 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart93 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c93
			var:flag_c93 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c93
			var:flag_c93 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart94 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c94
			var:flag_c94 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c94
			var:flag_c94 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart95 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c95
			var:flag_c95 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c95
			var:flag_c95 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart96 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c96
			var:flag_c96 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c96
			var:flag_c96 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart97 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c97
			var:flag_c97 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c97
			var:flag_c97 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart98 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c98
			var:flag_c98 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c98
			var:flag_c98 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart99 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c99
			var:flag_c99 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c99
			var:flag_c99 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}

ChronicleFlagWarPart100 = {
	type = character

	text = {
		trigger = {
			exists = var:flag_c100
			var:flag_c100 = flag:leader
		}
		localization_key = chronicle_contents_war_leader
	}
	text = {
		trigger = {
			exists = var:flag_c100
			var:flag_c100 = flag:ally
		}
		localization_key = chronicle_contents_war_ally
	}
}
