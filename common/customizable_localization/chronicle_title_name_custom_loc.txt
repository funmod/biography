﻿ChronicleTitleName1 = {
	type = character

	text = {
		trigger = {
			exists = var:title1
			var:title1 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.1
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName2 = {
	type = character

	text = {
		trigger = {
			exists = var:title2
			var:title2 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.2
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName3 = {
	type = character

	text = {
		trigger = {
			exists = var:title3
			var:title3 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.3
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName4 = {
	type = character

	text = {
		trigger = {
			exists = var:title4
			var:title4 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.4
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName5 = {
	type = character

	text = {
		trigger = {
			exists = var:title5
			var:title5 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.5
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName6 = {
	type = character

	text = {
		trigger = {
			exists = var:title6
			var:title6 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.6
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName7 = {
	type = character

	text = {
		trigger = {
			exists = var:title7
			var:title7 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.7
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName8 = {
	type = character

	text = {
		trigger = {
			exists = var:title8
			var:title8 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.8
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName9 = {
	type = character

	text = {
		trigger = {
			exists = var:title9
			var:title9 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.9
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName10 = {
	type = character

	text = {
		trigger = {
			exists = var:title10
			var:title10 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.10
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName11 = {
	type = character

	text = {
		trigger = {
			exists = var:title11
			var:title11 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.11
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName12 = {
	type = character

	text = {
		trigger = {
			exists = var:title12
			var:title12 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.12
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName13 = {
	type = character

	text = {
		trigger = {
			exists = var:title13
			var:title13 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.13
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName14 = {
	type = character

	text = {
		trigger = {
			exists = var:title14
			var:title14 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.14
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName15 = {
	type = character

	text = {
		trigger = {
			exists = var:title15
			var:title15 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.15
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName16 = {
	type = character

	text = {
		trigger = {
			exists = var:title16
			var:title16 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.16
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName17 = {
	type = character

	text = {
		trigger = {
			exists = var:title17
			var:title17 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.17
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName18 = {
	type = character

	text = {
		trigger = {
			exists = var:title18
			var:title18 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.18
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName19 = {
	type = character

	text = {
		trigger = {
			exists = var:title19
			var:title19 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.19
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName20 = {
	type = character

	text = {
		trigger = {
			exists = var:title20
			var:title20 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.20
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName21 = {
	type = character

	text = {
		trigger = {
			exists = var:title21
			var:title21 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.21
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName22 = {
	type = character

	text = {
		trigger = {
			exists = var:title22
			var:title22 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.22
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName23 = {
	type = character

	text = {
		trigger = {
			exists = var:title23
			var:title23 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.23
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName24 = {
	type = character

	text = {
		trigger = {
			exists = var:title24
			var:title24 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.24
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName25 = {
	type = character

	text = {
		trigger = {
			exists = var:title25
			var:title25 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.25
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName26 = {
	type = character

	text = {
		trigger = {
			exists = var:title26
			var:title26 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.26
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName27 = {
	type = character

	text = {
		trigger = {
			exists = var:title27
			var:title27 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.27
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName28 = {
	type = character

	text = {
		trigger = {
			exists = var:title28
			var:title28 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.28
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName29 = {
	type = character

	text = {
		trigger = {
			exists = var:title29
			var:title29 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.29
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName30 = {
	type = character

	text = {
		trigger = {
			exists = var:title30
			var:title30 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.30
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName31 = {
	type = character

	text = {
		trigger = {
			exists = var:title31
			var:title31 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.31
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName32 = {
	type = character

	text = {
		trigger = {
			exists = var:title32
			var:title32 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.32
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName33 = {
	type = character

	text = {
		trigger = {
			exists = var:title33
			var:title33 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.33
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName34 = {
	type = character

	text = {
		trigger = {
			exists = var:title34
			var:title34 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.34
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName35 = {
	type = character

	text = {
		trigger = {
			exists = var:title35
			var:title35 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.35
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName36 = {
	type = character

	text = {
		trigger = {
			exists = var:title36
			var:title36 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.36
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName37 = {
	type = character

	text = {
		trigger = {
			exists = var:title37
			var:title37 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.37
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName38 = {
	type = character

	text = {
		trigger = {
			exists = var:title38
			var:title38 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.38
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName39 = {
	type = character

	text = {
		trigger = {
			exists = var:title39
			var:title39 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.39
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName40 = {
	type = character

	text = {
		trigger = {
			exists = var:title40
			var:title40 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.40
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName41 = {
	type = character

	text = {
		trigger = {
			exists = var:title41
			var:title41 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.41
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName42 = {
	type = character

	text = {
		trigger = {
			exists = var:title42
			var:title42 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.42
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName43 = {
	type = character

	text = {
		trigger = {
			exists = var:title43
			var:title43 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.43
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName44 = {
	type = character

	text = {
		trigger = {
			exists = var:title44
			var:title44 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.44
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName45 = {
	type = character

	text = {
		trigger = {
			exists = var:title45
			var:title45 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.45
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName46 = {
	type = character

	text = {
		trigger = {
			exists = var:title46
			var:title46 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.46
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName47 = {
	type = character

	text = {
		trigger = {
			exists = var:title47
			var:title47 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.47
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName48 = {
	type = character

	text = {
		trigger = {
			exists = var:title48
			var:title48 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.48
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName49 = {
	type = character

	text = {
		trigger = {
			exists = var:title49
			var:title49 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.49
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName50 = {
	type = character

	text = {
		trigger = {
			exists = var:title50
			var:title50 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.50
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName51 = {
	type = character

	text = {
		trigger = {
			exists = var:title51
			var:title51 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.51
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName52 = {
	type = character

	text = {
		trigger = {
			exists = var:title52
			var:title52 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.52
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName53 = {
	type = character

	text = {
		trigger = {
			exists = var:title53
			var:title53 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.53
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName54 = {
	type = character

	text = {
		trigger = {
			exists = var:title54
			var:title54 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.54
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName55 = {
	type = character

	text = {
		trigger = {
			exists = var:title55
			var:title55 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.55
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName56 = {
	type = character

	text = {
		trigger = {
			exists = var:title56
			var:title56 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.56
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName57 = {
	type = character

	text = {
		trigger = {
			exists = var:title57
			var:title57 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.57
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName58 = {
	type = character

	text = {
		trigger = {
			exists = var:title58
			var:title58 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.58
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName59 = {
	type = character

	text = {
		trigger = {
			exists = var:title59
			var:title59 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.59
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName60 = {
	type = character

	text = {
		trigger = {
			exists = var:title60
			var:title60 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.60
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName61 = {
	type = character

	text = {
		trigger = {
			exists = var:title61
			var:title61 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.61
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName62 = {
	type = character

	text = {
		trigger = {
			exists = var:title62
			var:title62 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.62
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName63 = {
	type = character

	text = {
		trigger = {
			exists = var:title63
			var:title63 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.63
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName64 = {
	type = character

	text = {
		trigger = {
			exists = var:title64
			var:title64 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.64
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName65 = {
	type = character

	text = {
		trigger = {
			exists = var:title65
			var:title65 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.65
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName66 = {
	type = character

	text = {
		trigger = {
			exists = var:title66
			var:title66 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.66
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName67 = {
	type = character

	text = {
		trigger = {
			exists = var:title67
			var:title67 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.67
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName68 = {
	type = character

	text = {
		trigger = {
			exists = var:title68
			var:title68 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.68
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName69 = {
	type = character

	text = {
		trigger = {
			exists = var:title69
			var:title69 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.69
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName70 = {
	type = character

	text = {
		trigger = {
			exists = var:title70
			var:title70 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.70
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName71 = {
	type = character

	text = {
		trigger = {
			exists = var:title71
			var:title71 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.71
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName72 = {
	type = character

	text = {
		trigger = {
			exists = var:title72
			var:title72 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.72
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName73 = {
	type = character

	text = {
		trigger = {
			exists = var:title73
			var:title73 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.73
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName74 = {
	type = character

	text = {
		trigger = {
			exists = var:title74
			var:title74 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.74
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName75 = {
	type = character

	text = {
		trigger = {
			exists = var:title75
			var:title75 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.75
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName76 = {
	type = character

	text = {
		trigger = {
			exists = var:title76
			var:title76 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.76
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName77 = {
	type = character

	text = {
		trigger = {
			exists = var:title77
			var:title77 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.77
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName78 = {
	type = character

	text = {
		trigger = {
			exists = var:title78
			var:title78 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.78
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName79 = {
	type = character

	text = {
		trigger = {
			exists = var:title79
			var:title79 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.79
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName80 = {
	type = character

	text = {
		trigger = {
			exists = var:title80
			var:title80 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.80
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName81 = {
	type = character

	text = {
		trigger = {
			exists = var:title81
			var:title81 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.81
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName82 = {
	type = character

	text = {
		trigger = {
			exists = var:title82
			var:title82 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.82
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName83 = {
	type = character

	text = {
		trigger = {
			exists = var:title83
			var:title83 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.83
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName84 = {
	type = character

	text = {
		trigger = {
			exists = var:title84
			var:title84 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.84
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName85 = {
	type = character

	text = {
		trigger = {
			exists = var:title85
			var:title85 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.85
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName86 = {
	type = character

	text = {
		trigger = {
			exists = var:title86
			var:title86 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.86
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName87 = {
	type = character

	text = {
		trigger = {
			exists = var:title87
			var:title87 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.87
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName88 = {
	type = character

	text = {
		trigger = {
			exists = var:title88
			var:title88 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.88
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName89 = {
	type = character

	text = {
		trigger = {
			exists = var:title89
			var:title89 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.89
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName90 = {
	type = character

	text = {
		trigger = {
			exists = var:title90
			var:title90 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.90
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName91 = {
	type = character

	text = {
		trigger = {
			exists = var:title91
			var:title91 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.91
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName92 = {
	type = character

	text = {
		trigger = {
			exists = var:title92
			var:title92 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.92
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName93 = {
	type = character

	text = {
		trigger = {
			exists = var:title93
			var:title93 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.93
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName94 = {
	type = character

	text = {
		trigger = {
			exists = var:title94
			var:title94 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.94
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName95 = {
	type = character

	text = {
		trigger = {
			exists = var:title95
			var:title95 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.95
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName96 = {
	type = character

	text = {
		trigger = {
			exists = var:title96
			var:title96 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.96
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName97 = {
	type = character

	text = {
		trigger = {
			exists = var:title97
			var:title97 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.97
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName98 = {
	type = character

	text = {
		trigger = {
			exists = var:title98
			var:title98 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.98
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName99 = {
	type = character

	text = {
		trigger = {
			exists = var:title99
			var:title99 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.99
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}

ChronicleTitleName100 = {
	type = character

	text = {
		trigger = {
			exists = var:title100
			var:title100 = { tier >= tier_barony }
		}
		localization_key = chronicle_contents_title_name.100
	}
	text = {
		fallback = yes
		localization_key = chronicle_contents_title_name.0
	}
}
